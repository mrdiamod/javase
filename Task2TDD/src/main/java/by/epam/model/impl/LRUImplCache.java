package by.epam.model.impl;

import by.epam.Constants;
import by.epam.interfaces.ICache;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class LRUImplCache<K, V> extends LinkedHashMap<K, V> implements ICache<K, V> {
    private final int maxEntries;

    public LRUImplCache(int maxEnt) {
        super(maxEnt, Constants.DEF_LOAD_FACT, true);
        this.maxEntries = maxEnt;
    }

    public V get(Object key) {
        if (!super.containsKey(key)) {
            throw new NullPointerException();
        }
        return super.get(key);
    }

    @Override
    public Collection<V> getAll() {
        return super.values();
    }

    @Override
    public V put(K key) {
        return super.put(key, (V) (Integer) 1);
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return super.entrySet();
    }


    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > maxEntries;
    }
}
