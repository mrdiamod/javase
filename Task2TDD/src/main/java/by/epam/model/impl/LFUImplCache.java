package by.epam.model.impl;

import by.epam.Constants;
import by.epam.interfaces.ICache;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


public class LFUImplCache<K, V> extends LinkedHashMap<K, V> implements ICache<K, V> {
    private Map listQueue;
    private int maxEntries;

    public LFUImplCache(int maxEnt) {
        super(maxEnt, Constants.DEF_LOAD_FACT, false);
        this.maxEntries = maxEnt;
        listQueue = new LinkedHashMap(maxEntries, Constants.DEF_LOAD_FACT, true) {
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > maxEntries;
            }
        };
    }

    @Override
    public V get(Object key) {
        V value = null;
        if (super.containsKey(key)) {
            V valueInc = incValue(super.get(key));
            listQueue.put(key, valueInc);
            value = super.put((K) key, valueInc);
        }
        return value;
    }

    @Override
    public Collection<V> getAll() {
        return super.values();
    }

    @Override
    public V put(K key) {
        return put(key, (V) Integer.valueOf(1));
    }

    @Override
    public V put(K key, V value) {
        if (size() == maxEntries && super.get(key) == null) {
            deleteOldElementByLogicLFU();
        }
        listQueue.put(key, value);
        return super.put(key, value);
    }

    public V incValue(V param) {
        Integer value = (Integer) param;
        value++;
        return (V) value;
    }

    public void deleteOldElementByLogicLFU() {
        K searchKey = searchNeedfulFirstKeyByValue();
        listQueue.remove(searchKey);
        super.remove(searchKey);
    }

    //search needful first key by value is minimum elements > 1
    public K searchNeedfulFirstKeyByValue() {
        Integer minValue = Integer.MAX_VALUE;
        K resultByKey = null;
        Set<Map.Entry<K, V>> entrySet = listQueue.entrySet();
        for (Map.Entry<K, V> entry : entrySet) {
            Integer entValue = (Integer) entry.getValue();
            if (minValue > entValue) {
                minValue = entValue;
                resultByKey = entry.getKey();
            }
        }
        return resultByKey;
    }
}

