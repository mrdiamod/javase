package by.epam.interfaces;

import java.util.Collection;

public interface ICache<K,V> {
    V get(K key);
    Collection<V> getAll();
    V put(K key);
    V put(K key, V value);
    V remove(K key);
}
