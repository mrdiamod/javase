package by.epam;

public class Constants {
    public static final int DEF_INIT_CAP = 16;
    public static final float DEF_LOAD_FACT = 0.75f;
}
