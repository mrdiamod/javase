package by.epam.model.impl;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LRUImplCacheTest {
    private LRUImplCache cacheLRU;

    @Before
    public void init() {
        cacheLRU = new LRUImplCache(5);
        cacheLRU.put("b");
        cacheLRU.put("c", 44);
        cacheLRU.put("a");
        cacheLRU.put("d");
        cacheLRU.put("e");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("b");
        cacheLRU.get("a");
        cacheLRU.get("a");
        cacheLRU.get("d");
        cacheLRU.get("e");
        cacheLRU.get("e");
        cacheLRU.get("e");
        //edacedadceac
        cacheLRU.get("e");
        cacheLRU.get("d");
        cacheLRU.get("a");
        cacheLRU.get("c");
        cacheLRU.get("e");
        cacheLRU.get("d");
        cacheLRU.get("a");
        cacheLRU.get("d");
        cacheLRU.get("c");
        cacheLRU.get("e");
        cacheLRU.get("a");
        cacheLRU.get("c");
        cacheLRU.get("c");
        cacheLRU.get("c");
        cacheLRU.get("c");
        cacheLRU.get("d");
    }

    @Test
    public void get() {
        assertEquals(1, cacheLRU.get("a"));
        assertEquals(1, cacheLRU.get("b"));
        assertEquals(44, cacheLRU.get("c"));
        assertEquals(1, cacheLRU.get("d"));
    }

    @Test
    public void getAll() {
        assertNotNull(cacheLRU.getAll());
        assertEquals("[1, 1, 1, 44, 1]", cacheLRU.getAll().toString());
        cacheLRU.put("b", 33);
        assertEquals("[1, 1, 44, 1, 33]", cacheLRU.getAll().toString());
    }

    @Test
    public void put() {
        assertEquals(1, cacheLRU.get("a"));
        cacheLRU.put("x", 88);
        assertThrows(NullPointerException.class, () -> cacheLRU.get("1"));
        cacheLRU.put("f", 21);
        assertThrows(NullPointerException.class, () -> cacheLRU.get("e"));
        assertEquals(44, cacheLRU.get("c"));
        cacheLRU.put("c", 2);
        assertEquals(2, cacheLRU.get("c"));
    }

    @Test
    public void remove() {
        cacheLRU.put(4);
        assertEquals("{e=1, a=1, c=44, d=1, 4=1}", cacheLRU.toString());
        cacheLRU.put(5);
        assertEquals("{a=1, c=44, d=1, 4=1, 5=1}", cacheLRU.toString());
        cacheLRU.remove(5);
        assertEquals("{a=1, c=44, d=1, 4=1}", cacheLRU.toString());
        cacheLRU.remove(4);
        assertEquals("{a=1, c=44, d=1}", cacheLRU.toString());
        cacheLRU.remove(4);
        assertEquals("{a=1, c=44, d=1}", cacheLRU.toString());
        cacheLRU.remove(null);
        assertEquals("{a=1, c=44, d=1}", cacheLRU.toString());
        assertNull(cacheLRU.remove("x"));
    }
}