package by.epam.model.impl;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;


public class LFUImplCacheTest {
    private LFUImplCache cacheLFU;

    @Before
    public void init() {
        //It's real crush test if logic LFU bad
        cacheLFU = new LFUImplCache(5);
        cacheLFU.put("b");
        cacheLFU.put("c", 44);
        cacheLFU.put("a");
        cacheLFU.put("d");
        cacheLFU.put("e");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("b");
        cacheLFU.get("a");
        cacheLFU.get("a");
        cacheLFU.get("d");
        cacheLFU.get("e");
        cacheLFU.get("e");
        cacheLFU.get("e");
        //edacedadceac
        cacheLFU.get("e");
        cacheLFU.get("d");
        cacheLFU.get("a");
        cacheLFU.get("c");
        cacheLFU.get("e");
        cacheLFU.get("d");
        cacheLFU.get("a");
        cacheLFU.get("d");
        cacheLFU.get("c");
        cacheLFU.get("e");
        cacheLFU.get("a");
        cacheLFU.get("c");
        cacheLFU.get("c");
        cacheLFU.get("c");
        cacheLFU.get("c");
        cacheLFU.get("d");
    }

    @Test
    public void get() {
        assertEquals(6, cacheLFU.get("a"));
        assertEquals(10, cacheLFU.get("b"));
        assertEquals(50, cacheLFU.get("c"));
        assertEquals(6, cacheLFU.get("d"));
        assertEquals(7, cacheLFU.get("e"));
        cacheLFU.put("c", 8);
        assertEquals(8, cacheLFU.get("c"));
    }

    @Test
    public void getAll() {
        assertEquals("[10, 50, 6, 6, 7]", cacheLFU.getAll().toString());
        cacheLFU.put("c", 8); //after c=50 before c=8
        assertEquals("[10, 8, 6, 6, 7]", cacheLFU.getAll().toString());
    }

    @Test
    public void put() {
        assertNull(cacheLFU.get("Z"));
        cacheLFU.put("c", 6);
        assertEquals("{b=10, c=6, a=6, d=6, e=7}", cacheLFU.toString());
        cacheLFU.put("x", 9);//old element by step among  the least used is "a"
        assertEquals("{b=10, c=6, d=6, e=7, x=9}", cacheLFU.toString());
        cacheLFU.put("r");//old element by step among  the least used is d
        assertEquals("{b=10, c=6, e=7, x=9, r=1}", cacheLFU.toString());
        cacheLFU.put("s");
        assertEquals("{b=10, c=6, e=7, x=9, s=1}", cacheLFU.toString());
    }

    @Test
    public void remove() {
        assertEquals("{b=10, c=50, a=6, d=6, e=7}", cacheLFU.toString());
        cacheLFU.put(2, 1);
        assertEquals("{b=10, c=50, d=6, e=7, 2=1}", cacheLFU.toString());
        cacheLFU.put(1, 1);
        assertEquals("{b=10, c=50, d=6, e=7, 1=1}", cacheLFU.toString());
        cacheLFU.remove(2);
        assertEquals("{b=10, c=50, d=6, e=7, 1=1}", cacheLFU.toString());
        cacheLFU.remove(1);
        assertEquals("{b=10, c=50, d=6, e=7}", cacheLFU.toString());
        assertNull(cacheLFU.remove(1));
        assertNull(cacheLFU.get("1"));
        assertEquals("{b=10, c=50, d=6, e=7}", cacheLFU.toString());
        assertNull(cacheLFU.remove(null));
        assertEquals("{b=10, c=50, d=6, e=7}", cacheLFU.toString());
    }
}